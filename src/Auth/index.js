import auth0 from 'auth0-js';
import config from "../auth_config.json";

export default class Auth {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      // the following three lines MUST be updated
      domain: config.domain,
      audience: `https://${config.domain}/userinfo`,
      clientID: config.clientId,
      redirectUri: 'http://localhost:3000/callback',
      responseType: 'token id_token',
      scope: 'openid profile email'
    });

    this.getProfile = this.getProfile.bind(this);
    this.getUser = this.getUser.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.login = this.login.bind(this);
    this.loginGoogle = this.loginGoogle.bind(this);
    this.loginFB = this.loginFB.bind(this);
    this.signup = this.signup.bind(this);
    this.logout = this.logout.bind(this);
    this.changePassword = this.changePassword.bind(this);
    this.setSession = this.setSession.bind(this);
  }

  getProfile() {
    return this.profile;
  }

  getUser() {
    return this.user;
  }

  handleAuthentication() {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (err) return reject(err);
        if (!authResult || !authResult.idToken) {
          return reject(err);
        }

        this.auth0.client.userInfo(authResult.accessToken, function(err, user){
          if (err) return reject(err);
          this.user = user;

          this.setSession(authResult);
          resolve();
          }.bind(this));
        
      });
    })
  }

  isAuthenticated() {
    return new Date().getTime() < this.expiresAt;
  }

  login() {
    this.auth0.login({
        email: "dpsol@yahoo.com",
        password: "zxasQW12",
        realm: "Username-Password-Authentication",
    })
  }

  loginGoogle() {
    this.auth0.authorize({
      connection: "google-oauth2"
    })
  }
  
  loginFB() {
    this.auth0.authorize({
      connection: "facebook"
    })
  }

  signup(email, password) {
    this.auth0.signup({
        email: email,
        password: password,
        username: "test1",
        connection: "Username-Password-Authentication",
        user_metadata: {firstName: "First", lastName: "Last"}
    }, function(err) {
        if (err) return alert('Something went wrong: ' + err.message);
        return alert('success signup without login!');
    })
  }

  logout() {
    // clear id token, user and expiration
    this.idToken = null;
    this.expiresAt = null;
    this.user = null
  }

  changePassword() {
    this.auth0.changePassword({
        connection: "Username-Password-Authentication",
        email: "dpsol@yahoo.com",
    }, function (err, resp) {
        if(err){
          console.log(err.message);
        }else{
          console.log(resp);
        }
    });
  }

  setSession(authResult) {
    this.idToken = authResult.idToken;
    this.profile = authResult.idTokenPayload;
    // set the time that the id token will expire at
    this.expiresAt = authResult.expiresIn * 1000 + new Date().getTime();
  }
}